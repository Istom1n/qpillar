package Istom1n.qPillar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class MainQP extends JavaPlugin {

   
   public static MainQP plugin;
   public String prefix = "[qPillar] ";
   public final Logger logger = Logger.getLogger("Minecraft");
   
   
   public int height = 8;
   public int warnheight = 5;
   public int above = 62;
   public String world = "cake";
   private Boolean delete = true;
   private Boolean ban = true;
   private String cmdstart = "tempban";
   private String cmdend = "1 hour stolb";
   public List blackList = new ArrayList(1024); 

   public void onDisable() {
      PluginDescriptionFile pdfFile = this.getDescription();
      this.logger.info(this.prefix + "Version " + pdfFile.getVersion() + " by Istom1n: Disabled");
   }

   public void onEnable() {      
try {
         this.loadConfiguration();
         
      } catch (IOException var2) {
         this.logger.info("[qPillar] Config load failed.");
      }
      this.getServer().getPluginManager().registerEvents(new bListener(this), this);
      PluginDescriptionFile pdfFile = this.getDescription();
      this.logger.info(this.prefix + "Version " + pdfFile.getVersion() + " by Istom1n");
   }


   
    public void loadConfiguration() throws IOException {
      File cfgFile = new File(this.getDataFolder() + "/options.yml");
      YamlConfiguration config = YamlConfiguration.loadConfiguration(cfgFile);
      
      config.addDefault("heightOfPillar", Integer.valueOf(height));
      config.addDefault("WarningHeight", Integer.valueOf(warnheight));
      config.addDefault("pillarAbove", Integer.valueOf(above));
      config.addDefault("world", String.valueOf(world));
      config.addDefault("deletePillar", Boolean.valueOf(delete));
      Integer[] bl = new Integer[]{Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(5), Integer.valueOf(13), Integer.valueOf(18), Integer.valueOf(87)};
      config.addDefault("blacklist", Arrays.asList(bl));
      config.addDefault("ban", Boolean.valueOf(ban));
      config.addDefault("BanCommandStart", String.valueOf(cmdstart));
      config.addDefault("BanCommandEnd", String.valueOf(cmdend));
      
      
      config.options().copyDefaults(true);
      config.save(cfgFile);
      
      this.height = config.getInt("heightOfPillar");
      this.warnheight = config.getInt("WarningHeight");
      this.above = config.getInt("pillarAbove");
      this.world = config.getString("world");
      this.delete = config.getBoolean("deletePillar");
      this.ban = config.getBoolean("ban");
      this.cmdstart = config.getString("BanCommandStart");
      this.cmdend = config.getString("BanCommandEnd");
      this.blackList = config.getIntegerList("blacklist");
      
      this.logger.info("Restricted pillars blocks: " + blackList);
      
   }
   
   
    
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
    Player p = (Player)sender; 
    
    if (p.hasPermission("pillar.reload")||p.isOp()){
    if (commandLabel.equals("prl")) {  
        
        try {
         this.loadConfiguration();
         p.sendMessage(ChatColor.GREEN + "Config loaded.");
         
      } catch (IOException var2) {
         this.logger.info("[qPillar] Config load failed.");
         p.sendMessage(ChatColor.RED + "Config load failed.");
      }
        
        p.sendMessage(ChatColor.GREEN + "Restricted pillars blocks: " + ChatColor.YELLOW + blackList.toString());
        p.sendMessage(ChatColor.GREEN + "The maximum height of the column: " + ChatColor.YELLOW + height);
        }
    else{
    p.sendMessage(ChatColor.RED + "You don't have permission.");
    }
        
        return true;
     }
  
    

  return false;
  }
    
    
   
   
   public boolean ifPillarBlock(Block b) {
      return (blackList.contains(b.getRelative(0, -1, 0).getTypeId())) && (b.getRelative(1, 0, 0) == null || b.getRelative(1, 0, 0).getTypeId() == 0) && (b.getRelative(-1, 0, 0) == null || b.getRelative(-1, 0, 0).getTypeId() == 0) && (b.getRelative(0, 0, 1) == null || b.getRelative(0, 0, 1).getTypeId() == 0) && (b.getRelative(0, 0, -1) == null || b.getRelative(0, 0, -1).getTypeId() == 0);
   }

   public boolean checkForPillar(Player p, Block b, Integer s, Integer a) {
      if(b.getY() >= a.intValue()) {
         switch(s.intValue()) {
         case 0:
            if(this.ifPillarBlock(b)) {
               return true;
            }
            break;
         case 1:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0))) {
               return true;
            }
            break;
         case 2:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0))) {
               return true;
            }
            break;
         case 3:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0))) {
               return true;
            }
            break;
         case 4:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0))) {
               return true;
            }
            break;
         case 5:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0)) && this.ifPillarBlock(b.getRelative(0, -5, 0))) {
               return true;
            }
            break;
         case 6:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0)) && this.ifPillarBlock(b.getRelative(0, -5, 0)) && this.ifPillarBlock(b.getRelative(0, -6, 0))) {
               return true;
            }
            break;
         case 7:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0)) && this.ifPillarBlock(b.getRelative(0, -5, 0)) && this.ifPillarBlock(b.getRelative(0, -6, 0)) && this.ifPillarBlock(b.getRelative(0, -7, 0))) {
               return true;
            }
            break;
         case 8:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0)) && this.ifPillarBlock(b.getRelative(0, -5, 0)) && this.ifPillarBlock(b.getRelative(0, -6, 0)) && this.ifPillarBlock(b.getRelative(0, -7, 0)) && this.ifPillarBlock(b.getRelative(0, -8, 0))) {
               return true;
            }
            break;
         case 9:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0)) && this.ifPillarBlock(b.getRelative(0, -5, 0)) && this.ifPillarBlock(b.getRelative(0, -6, 0)) && this.ifPillarBlock(b.getRelative(0, -7, 0)) && this.ifPillarBlock(b.getRelative(0, -8, 0)) && this.ifPillarBlock(b.getRelative(0, -9, 0))) {
               return true;
            }
            break;
         case 10:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0)) && this.ifPillarBlock(b.getRelative(0, -5, 0)) && this.ifPillarBlock(b.getRelative(0, -6, 0)) && this.ifPillarBlock(b.getRelative(0, -7, 0)) && this.ifPillarBlock(b.getRelative(0, -8, 0)) && this.ifPillarBlock(b.getRelative(0, -9, 0)) && this.ifPillarBlock(b.getRelative(0, -10, 0))) {
               return true;
            }
            break;
         case 11:
            if(this.ifPillarBlock(b) && this.ifPillarBlock(b.getRelative(0, -1, 0)) && this.ifPillarBlock(b.getRelative(0, -2, 0)) && this.ifPillarBlock(b.getRelative(0, -3, 0)) && this.ifPillarBlock(b.getRelative(0, -4, 0)) && this.ifPillarBlock(b.getRelative(0, -5, 0)) && this.ifPillarBlock(b.getRelative(0, -6, 0)) && this.ifPillarBlock(b.getRelative(0, -7, 0)) && this.ifPillarBlock(b.getRelative(0, -8, 0)) && this.ifPillarBlock(b.getRelative(0, -9, 0)) && this.ifPillarBlock(b.getRelative(0, -10, 0)) && this.ifPillarBlock(b.getRelative(0, -11, 0))) {
               return true;
            }
         }
      }

      return false;
   }

   public boolean actOnPillar(Player p, Block b) {
      if(delete) {
         for(Integer blockY = Integer.valueOf(height); blockY.intValue() > -1; blockY = Integer.valueOf(blockY.intValue() - 1)) {
            if(blockY.intValue() != 0) {
               b.getRelative(0, -blockY.intValue(), 0).setTypeId(0);
            } else {
               b.setTypeId(0);
            }
         }
      }


      if(ban) {
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), cmdstart + " " + p.getName() + " " +  cmdend);  
      }

      return false;
   }
}
