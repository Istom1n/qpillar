package Istom1n.qPillar;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class bListener implements Listener {

   public static MainQP plugin;


   public bListener(MainQP instance) {
      plugin = instance;
   }

   @EventHandler
   public void onBlockPlace(BlockPlaceEvent e) {
      Block b = e.getBlock();
      Player p = e.getPlayer();
      if (p.getWorld().getName().equals(plugin.world)){
      
      if(plugin.checkForPillar(p, b, Integer.valueOf(plugin.warnheight), Integer.valueOf(plugin.above))) {
         p.sendMessage(ChatColor.DARK_RED + "If you don't stop to build a column, you will be banned!");
      }
      
      if(plugin.checkForPillar(p, b, Integer.valueOf(plugin.height), Integer.valueOf(plugin.above))) {
         plugin.actOnPillar(p, b);
      }
      }
   }

   
   
}

